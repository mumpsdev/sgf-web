import { dragula } from './dragula.class';
import { Injectable, EventEmitter } from '@angular/core';
import { $ } from 'protractor';
import { setTimeout } from 'timers';

@Injectable()
export class DragulaService {
  constructor(
  ){

  }
  public cancel: EventEmitter<any> = new EventEmitter();
  public cloned: EventEmitter<any> = new EventEmitter();
  public drag: EventEmitter<any> = new EventEmitter();
  public dragend: EventEmitter<any> = new EventEmitter();
  public drop: EventEmitter<any> = new EventEmitter();
  public out: EventEmitter<any> = new EventEmitter();
  public over: EventEmitter<any> = new EventEmitter();
  public remove: EventEmitter<any> = new EventEmitter();
  public shadow: EventEmitter<any> = new EventEmitter();
  public dropModel: EventEmitter<any> = new EventEmitter();
  public removeModel: EventEmitter<any> = new EventEmitter();
  public dropUnique: EventEmitter<any> = new EventEmitter();
  public notDrop: EventEmitter<any> = new EventEmitter();
  private events: string[] = [
    'cancel', 'cloned', 'drag', 'dragend', 'drop', 'out', 'over',
    'remove', 'shadow', 'dropModel', 'removeModel'
  ];
  private bags: any[] = [];
  private sourceClick: any;
  private dropIdUnique: boolean = false;
  private listNotDrop: Array<string> = [];
  private inDrop = false;

  public setSourceClick(value: any){
    this.sourceClick = value;
  }

  public getSourceClick(){
    return this.sourceClick;
  }

  public setDropIdUnique(value: boolean){
      this.dropIdUnique = value;
  }

  public addNotDrop(name: string){
    if(!this.listNotDrop.includes(name)){
      this.listNotDrop.push(name);
    }
  }

  public removeNotDrop(name: string){
    if(this.listNotDrop.includes(name)){
      this.listNotDrop.splice(this.listNotDrop.indexOf(name), 1);
    }
  }

  public getDropIdUnique(){
      return this.dropIdUnique;
  }

  public add(name: string, drake: any): any {
    let bag = this.find(name);
    if (bag) {
      throw new Error('Bag named: "' + name + '" already exists.');
    }
    bag = {name, drake};
    this.bags.push(bag);
    if (drake.models) { // models to sync with (must have same structure as containers)
      this.handleModels(name, drake);
    }
    if (!bag.initEvents) {
      this.setupEvents(bag);
    }
    return bag;
  }

  public find(name: string): any {
    for (let bag of this.bags) {
      if (bag.name === name) {
        return bag;
      }
    }
  }

  public destroy(name: string): void {
    let bag = this.find(name);
    let i = this.bags.indexOf(bag);
    this.bags.splice(i, 1);
    bag.drake.destroy();
  }

  public setOptions(name: string, options: any): void {
    let bag = this.add(name, dragula(options));
    this.handleModels(name, bag.drake);
  }

  private handleModels(name: string, drake: any): void {
    let dragElm: any;
    let dragIndex: number;
    let dropIndex: number;
    let sourceModel: any;

    drake.on('remove', (el: any, source: any) => {
      if (!drake.models) {
        return;
      }
      sourceModel = drake.models[drake.containers.indexOf(source)];
      sourceModel.splice(dragIndex, 1);
      this.removeModel.emit([name, el, source]);
    });

    drake.on('drag', (el: any, source: any) => {
      dragElm = el;
      dragIndex = this.domIndexOf(el, source);
    });

    drake.on('drop', (dropElm: any, target: any, source: any) => {
      if (!drake.models || !target) {
        return;
      }

      if(!this.inDrop){
        this.inDrop = true;
        // Se a o grupo de bags não estiver na lista que não pode drop
        if(!this.listNotDrop.includes(name)){
          dropIndex = this.domIndexOf(dropElm, target);
          if(this.sourceClick){
            sourceModel = drake.models[drake.containers.indexOf(this.sourceClick)];
          }else{
            sourceModel = drake.models[drake.containers.indexOf(source)];
          }
          let targetModel = [];
          let dropElmModel = {};
          if (target === source || target == this.sourceClick) {
            sourceModel.splice(dropIndex, 0, sourceModel.splice(dragIndex, 1)[0]);
          }else {
            let notCopy = dragElm === dropElm;
            targetModel = drake.models[drake.containers.indexOf(target)];
            dropElmModel = notCopy ? sourceModel[dragIndex] : JSON.parse(JSON.stringify(sourceModel[dragIndex]));
            if (notCopy) {
              sourceModel.splice(dragIndex, 1);
            }
            targetModel.splice(dropIndex, 0, dropElmModel);
            target.removeChild(dropElm); // element must be removed for ngFor to apply correctly
          }
          this.dropModel.emit([name, dropElm, dropElmModel, source, target, sourceModel, targetModel]);
        }else{
          source.append(dropElm);
          this.notDrop.emit([dropElm, target, source]);
        }
        setTimeout(() => {
          this.inDrop = false;
        }, 500);
      }
    });
  }

  public getIndexSource(nameBag: string, source: any): number{
    let bag = this.find(nameBag);
    return bag.drake.containers.indexOf(source);
  }

  public moveElementTarget(name: string, sourceElement: any, source: any, target: any){
    let bag = this.find(name);
    let sourceIndex = this.domIndexOf(sourceElement, source);
    let sourceModels = bag.drake.models[bag.drake.containers.indexOf(source)];
    let sourceBag = bag.drake.containers[bag.drake.containers.indexOf(source)];
    let sourceElementModel = sourceModels[sourceIndex];
    sourceModels.splice(sourceIndex, 1);
    sourceBag.removeChild(sourceElement);
    let targetBag = bag.drake.containers[bag.drake.containers.indexOf(target)];
    targetBag.addChild(sourceElement);
    let targetModels = bag.drake.models[bag.containers.indexOf(target)];
    targetModels.push(sourceElementModel);
  }

  private setupEvents(bag: any): void {
    bag.initEvents = true;
    let that: any = this;
    let emitter = (type: any) => {
      function replicate(): void {
        let args = Array.prototype.slice.call(arguments);
        that[type].emit([bag.name].concat(args));
      }

      bag.drake.on(type, replicate);
    };
    this.events.forEach(emitter);
  }

  private domIndexOf(child: any, parent: any): any {
    return Array.prototype.indexOf.call(parent.children, child);
  }
}
