export const URLs = {//Urls rest.
    //"localhost": "https://sgf.mumps.com.br",
     "localhost": "http://localhost:3000",
     "AUTH_LOGIN": "/api/v1/getLoginAuth/:login",
     "AUTH_AUTHENTICATE": "/api/v1/authenticate",
     "AUTH_LOGGED": "/api/v1/authLogged",
     "AUTH_ALL": "/*",
     //Usuário
     "USUARIO": "/api/v1/usuario",
     "USUARIO_ID": "/api/v1/usuario/:id",
     "USUARIO_PAGINATION": "/api/v1/usuario/pagination",
     "USUARIO_FILTER": "/api/v1/usuario/filter",
     //Organizacao
     "ORGANIZACAO": "/api/v1/organizacao",
     "ORGANIZACAO_ID": "/api/v1/organizacao/:id",    
     //Filial
     "FILIAL": "/api/v1/filial",
     "FILIAL_ID": "/api/v1/filial/:id",     
     //Perfil
     "PERFIL": "/api/v1/perfil",
     "PERFIL_ID": "/api/v1/perfil/:id",    
     //Acao
     "ACAO": "/api/v1/acao",
     "ACAO_NOTIN": "/api/v1/acao/notin",
     "ACAO_ID": "/api/v1/acao/:id",
     //Colaborador
     "COLABORADOR": "/api/v1/colaborador",
     "COLABORADOR_ID": "/api/v1/colaborador/:id",    
     "COLABORADOR_FILIAIS": "/api/v1/colaborador/filiais",
     //Escala
     "LIST_ANOS_COM_ESCALA_FILIAL" : "/api/v1/escala/filial/:filial",
     "LIST_ESCALAS_ANO_FILIAL": "/api/v1/escala/ano/:ano/filial/:filial",
     "SHOW_ESCALA_MES": "/api/v1/escala/ano/:ano/mes/:mes/filial/:filial",
     "INICIAR_ESCALA_MES": "/api/v1/escala/iniciar",
     "GERAR_ESCALA_MES": "/api/v1/escala/gerar",
     "ESCALA_MUDAR_TIPO_TRABALHO_DIA_COLABORADOR": "/api/v1/escala/mudarTipoTrabalho",
     "ESCALA_MUDAR_STATUS" : '/api/v1/escala/mudarStatus',
     //Tipo Documento
     "TIPO_DOCUMENTO": "/api/v1/tipodocumento",
     "TIPO_DOCUMENTO_ID": "/api/v1/tipodocumento/:id", 
     //Tipo Contato
     "TIPO_CONTATO": "/api/v1/tipocontato",
     "TIPO_CONTATO_ID": "/api/v1/tipocontato/:id",  
     //Contato
     "CONTATO": "/api/v1/contato",
     "CONTATO_ID": "/api/v1/contato/:id",
     //Documento
     "DOCUMENTO": "/api/v1/documento",
     "DOCUMENTO_ID": "/api/v1/documento/:id",
     //Feriado
     "FERIADO": "/api/v1/feriado",
     "FERIADO_ID": "/api/v1/feriado/:id"    
}

export const tMsg = {//Tipos de mensagens
    "DANGER": "msgErro",
    "SUCCESS": "msgSuccesso",
    "INFO": "msgInfo",
    "ALERT": "msgAlert"
}

export const oMsg = {//Objetos de retorno
    "OBJ": "obj",
    "COLABORADOR": "colaborador",
    "ACOES": "acoes",
    "LIST": "list",
    "SEARCH": "search",
    "LIST_MSG": "listMsg",
    "PAGE": "page",
    "TOTAL_PAGES": "totalPages",
    "FIELD_SORT": "fieldSort",
    "SORT": "sort",
    "LIMIT": "limit",
    "RANGE_START": "rangeStart",
    "RANGE_END": "rangeEnd",
    "TOTAL_ROWS": "totalRows",
}

export const secretToken ={
    "TOKEN": "x-auth-token",
    "SECRET_PUBLIC" : "projeto-publico"
}