import { TranslateService } from './../util/translate/translate.service';
import { hmacSHA512 } from 'crypto-js/hmac-sha512';
import { Observable } from 'rxjs/Observable';
import { URLs, oMsg, secretToken, tMsg } from './../util/values';
import { AppComponent } from './../app.component';
import {
     Injectable,
     ComponentRef,
 } from '@angular/core';
import * as cryptojs from "crypto-js";
import * as dateFns from "date-fns";

@Injectable()
export class UtilService {
    constructor(
        private app: AppComponent,
        private translateService: TranslateService        
    ) { 
    }

    public hMacSha512(text: string):string{
        try {
            let hash = cryptojs.HmacSHA512(text, secretToken.SECRET_PUBLIC);
            return hash.toString();
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public encrypt(text: string):string{
        try {
            let ciphertext = cryptojs.AES.encrypt(JSON.stringify(text), secretToken.SECRET_PUBLIC);
            return ciphertext;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public decrypt(ciphertext: any):string{
        try {
            let bytes  = cryptojs.AES.decrypt(ciphertext.toString(), secretToken.SECRET_PUBLIC);
            var decryptedData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));
            return decryptedData;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + ciphertext);
            return "";
        }
    }

    public getOnlyNumbers(value){
        if(value){
            try {
                return value.replace(/[^0-9]+/g, "");
            } catch (e) {console.log(e);}
        }
        return null;
    };
  
    public getMsgStatusError(status){
        let msg: string = this.translateService.getValue("statusDefault");
        switch (status) {
            case 0: msg = this.translateService.getValue("status0"); break;
            case 302: msg = this.translateService.getValue("status302"); break;
            case 304: msg = this.translateService.getValue("status304"); break;
            case 400: msg = this.translateService.getValue("status400"); break;
            case 401: msg = this.translateService.getValue("status401"); break;
            case 403: msg = this.translateService.getValue("status403"); break;
            case 404: msg = this.translateService.getValue("status404"); break;
            case 405: msg = this.translateService.getValue("status405"); break;
            case 410: msg = this.translateService.getValue("status410"); break;
            case 500: msg = this.translateService.getValue("status500"); break;
            case 502: msg = this.translateService.getValue("status302"); break;
            case 503: msg = this.translateService.getValue("status302"); break;
            default: break;
        }
        return msg;
        
    };
  
    public getItemList = (lista, value, campo) => {
        if(!campo){campo = "id"};
        var item = null;
        if(value && lista){
            for(var index = 0; index < lista.length; index++){
                if(lista[index][campo] == value){
                    item = lista[index];
                    break;
                }
            }
        }
        return item;
    };

    public getCountDuplicateList = (lista, value, campo) => {
        let count = 0;
        if(!campo){campo = "id"};
        var item = null;
        if(value && lista){
            for(var index = 0; index < lista.length; index++){
                if(lista[index][campo] == value){
                    count++;
                }
            }
        }
        return count;
    };
  
    public removeItemList = (lista, value, campo) =>{
        if(!campo){campo = "id"};      
        var item = null;
        if(value && lista){
            for(var index = 0; index < lista.length; index++){
                if(lista[index][campo] == value){
                    lista.splice(index, 1);
                    break;
                }
            }
        }
    };
  
    public filterItemList = (lista, value:string, campo:string) => {
        if(!campo){campo = "id"};
        if(!value) return lista;
        return lista.filter(item => { 
            return  item[campo].toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) != -1;
        });
    };

    public changeItemList(oldList: Array<any>, newList: Array<any>, item: any){
        if(oldList && newList){
          let index = oldList.indexOf(item);
          if(index > -1){
            oldList.splice(index, 1);
          }
          newList.push(item);
        }
    }

    public getValueObjectField(obj: object, field: string){
        if(field.includes(".")){
            let fields = field.split(".");
            fields.forEach((fieldSplit) => {
              obj = obj[fieldSplit];
            });
            return obj;
        }
        return obj[field] || "";
    }

    public getDayInMonth(year: number, dayYear): number{
        var result = dateFns.setDayOfYear(new Date(year, 0 ,1 ), dayYear);
        return parseInt(dateFns.format(result, 'DD'));
    }
}