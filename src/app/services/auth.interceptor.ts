import { Router } from '@angular/router';
import { AppComponent } from './../app.component';
import { UtilService } from './util.service';
import { secretToken, tMsg, oMsg } from './../util/values';
import { Injectable } from '@angular/core';
import { 
    HttpEvent, 
    HttpInterceptor, 
    HttpHandler, 
    HttpRequest,
    HttpResponse,
    HttpResponseBase,
    HttpErrorResponse 
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let newReq = this.setTokenSessionStory(req);
        return next.handle(newReq).do(res => {
            if (res instanceof HttpResponse) {
            }
        }, error => {
            if(error instanceof HttpErrorResponse){
                
                sessionStorage.removeItem(secretToken.TOKEN);
                if(error.status == 401){
                }
            }
        });
    }

    private setTokenSessionStory(req: HttpRequest<any>): HttpRequest<any>{
        let token = sessionStorage.getItem(secretToken.TOKEN);
        if(token && token != "undefined"){
            let newReq = req.clone({
                headers: req.headers.set(secretToken.TOKEN, token)
            });
            return newReq;
        }else{
            return req;
        }
    }
    
    private saveTokenSessionStory(res: HttpResponse<any>): void{
        let token = res.headers[secretToken.TOKEN]
        if(token){
            sessionStorage.setItem(secretToken.TOKEN, token);
        }
    }
}