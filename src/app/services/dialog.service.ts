import { TranslateService } from './../util/translate/translate.service';
import { UtilService } from './util.service';
import { secretToken, tMsg, oMsg } from './../util/values';
import {
    Injectable,
    ComponentRef,
    ComponentFactoryResolver,
} from '@angular/core';
import {
    MzModalService,
    MzBaseModal,
    MzModalComponent,
    MzToastService,
} from 'ng2-materialize';
import {HttpErrorResponse} from '@angular/common/http';
//Modals
import { LoadingComponent } from './../pages/custom-components/loading/loading.component';
import { ConfirmComponent } from './../pages/custom-components/confirm/confirm.component';
import { AlertComponent } from './../pages/custom-components/alert/alert.component';
import { MenuRight } from '../pages/custom-components/menu-right/menu-right.component';

@Injectable()
export class DialogService {
    constructor(
        private mzModalService: MzModalService,
        private mzToastService: MzToastService,
        private translateService: TranslateService,
        private utilService: UtilService,
        private factoryResolver: ComponentFactoryResolver
    ) {
        this.tituloModal = this.translateService.getValue("MENSAGEM");
    }

    private tituloModal: string = this.translateService.getValue("MENSAGEM");
    public loading: ComponentRef<MzBaseModal>;
    public listMenu: Array<MenuRight> = new Array();
    private isLoading = false;

    public alert(title: string, message: string, callBackOK: any){
        if(!message){message = ""}
        if(!title){title = this.tituloModal}
        if(!callBackOK){callBackOK = null}
        AlertComponent.title = title;
        AlertComponent.message = message;
        AlertComponent.callbackOk = callBackOK;
        let modal = this.mzModalService.open(AlertComponent);
    }
    
    public confirm(title: string, message: string, callbackOk: any, callbackCancel: any){
        if(!message){message = ""}
        if(!title){title = this.tituloModal}
        if(!callbackOk){callbackOk = null}
        if(!callbackCancel){callbackCancel = null}
        ConfirmComponent.title = title;
        ConfirmComponent.message = message;
        ConfirmComponent.callbackOk = callbackOk;
        ConfirmComponent.callbackCancel = callbackCancel;
        let modal = this.mzModalService.open(ConfirmComponent);
    }

    public openLoading(message: string, duration: number){
        if(!this.isLoading){
            this.isLoading = true;
            if(!message){message = this.translateService.getValue("AGUARDE") + "..."}
            LoadingComponent.message = message;
            setTimeout(() => {
                this.loading = this.mzModalService.open(LoadingComponent);
            }, 100);
            if(duration > 0){
                setTimeout(() => {
                    this.closeLoading();
                }, duration);
            }
        }
    }
    
    public closeLoading(){
        setTimeout(() => {
            if(this.loading){
                this.loading.instance.modalComponent.close();
                this.isLoading = false;
            }
        }, 100);
    }

    public setMessageLoading(message: string){
        LoadingComponent.message = message;
    }

    
    public toast(message: string, duration: number, addClass: string, callbackFinish){
        if(!addClass){
            addClass = "black";
        }
        this.mzToastService.show(message, duration, addClass, callbackFinish);
    }
    
    public showErrorModal(error:any){
        this.closeLoading();
        sessionStorage.removeItem(secretToken.TOKEN);
        if(error instanceof HttpErrorResponse){
            let msg = this.utilService.getMsgStatusError(error.status);
            this.alert(this.tituloModal, msg, null);
        }
    }

    public showMsgData = function(data){
        let msgs = data[oMsg.LIST_MSG];
        let msgsModal = "";
        if(msgs){
            msgs.forEach(m => {
                 let isToast = false;
                 let msg: string;
                 if(m.type == tMsg.INFO){isToast = true; msg = m.msg;}
                 else if(m.type == tMsg.SUCCESS){isToast = true; msg = m.msg;}
                 else if(m.type == tMsg.DANGER){msg = m.msg;}
                 else if(m.type == tMsg.ALERT){msg = m.ms;}
                 msg = this.translateService.getValue(msg);
                 if(isToast){
                     this.toast(msg, 5000, null, null);
                 }else{
                    msgsModal += msg + "\n";
                }
            });
            if(msgsModal){
                this.alert(this.tituloModal, msgsModal);
            }
        }
        this.closeLoading();
    };

    public addMenuRight(menu: MenuRight){
        this.listMenu.push(menu);
    }

    closeAllMenuRight(menuAtual: MenuRight){
        this.listMenu.forEach((menu) => {
            if(menu != menuAtual){
                menu._isShow = false;
            }
        })
    }

    isMenuRightOpen(menuNow: MenuRight): boolean{
        let isOpen = this.listMenu.some((menu) => 
            menuNow != menu && menu._isShow && menu.fixed
        );
        return isOpen;
    }
}