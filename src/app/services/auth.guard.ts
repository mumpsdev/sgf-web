import { HttpClient } from '@angular/common/http';
import { secretToken, URLs, oMsg } from './../util/values';
import { Injectable, OnInit } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from "@angular/router"
import {Observable} from "rxjs/Rx";
import {UtilService} from "./util.service";
//Paginas
import { AppComponent } from './../app.component';


@Injectable()
export class AuthGuard implements CanActivate{

  constructor(
    private router: Router,
    private app: AppComponent,
    private http: HttpClient,
    private utilService: UtilService
  ) { }

  canActivate (
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot,

  ): Observable<boolean> | boolean{
        let token = sessionStorage.getItem(secretToken.TOKEN);
        if(!token || token == "undefined"){
          sessionStorage.removeItem(secretToken.TOKEN);
          this.router.navigate(["/login"]);
          this.app.setIsLogged(false);
          return false;
        }
     return true;
  }

  private getUser(){

  }

}
