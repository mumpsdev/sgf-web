import { Router, ActivatedRoute } from '@angular/router';
import { URLs, oMsg } from './../../../util/values';
import { DialogService } from './../../../services/dialog.service';
import { TranslateService } from './../../../util/translate/translate.service';
import { UtilService } from './../../../services/util.service';
import { AppComponent } from './../../../app.component';
import { ngIfFade } from './../../../util/animates.custons';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cadastro-organizacao',
  templateUrl: './cadastro-organizacao.component.html',
  styleUrls: ['./cadastro-organizacao.component.scss'],
  animations: [ngIfFade]
})
export class CadastroOrganizacaoComponent implements OnInit {

  constructor(
    private app: AppComponent,
    private translateService: TranslateService,
    private http: HttpClient,
    private dialogService: DialogService,
    private router:Router,
    private route: ActivatedRoute

  ) { }
  organizacao = {cnpj: "", nomeFantasia: ""};
  public idOrganizacao: any;

  ngOnInit() {
    let tituloOrganizacao = "TITLE_CAD_ORGANIZACAO";
    this.route.params.subscribe((params) => {
      this.idOrganizacao = params["id"];
    });
    if(this.idOrganizacao){
      tituloOrganizacao = "TITLE_EDIT_ORGANIZACAO";
      this.carregarOrganizacao();
    }
    tituloOrganizacao = this.translateService.getValue(tituloOrganizacao);
    this.app.setTitle(tituloOrganizacao);
    console.log(this.idOrganizacao);
  }

  public carregarOrganizacao(){
    this.dialogService.openLoading(null, 0);
    this.http.get(URLs.localhost + URLs.ORGANIZACAO + "/" + this.idOrganizacao).subscribe(data => {
      this.dialogService.closeLoading();
        this.organizacao = data[oMsg.OBJ];
        this.dialogService.showMsgData(data);
      }, error => {
        this.dialogService.showErrorModal(error);
    });
  }

  public atualizar(){
    this.http.put(URLs.localhost + URLs.ORGANIZACAO + "/" + this.idOrganizacao, this.organizacao).subscribe(data => {
      this.dialogService.closeLoading();
        this.router.navigate(["/organizacao"])        
        this.dialogService.showMsgData(data);
      }, error => {
        this.dialogService.showErrorModal(error);
    });
  }

  public salvar(){
      this.http.post(URLs.localhost + URLs.ORGANIZACAO, this.organizacao).subscribe(data => {
        this.dialogService.closeLoading();
          this.router.navigate(["/organizacao"])        
          this.dialogService.showMsgData(data);
        }, error => {
          this.dialogService.showErrorModal(error);
      });
  }

  limpar(){
    this.organizacao = {cnpj: "", nomeFantasia: ""};
  }

}
