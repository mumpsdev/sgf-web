import { TranslateService } from './../../../util/translate/translate.service';
import { PagesModule } from './../../pages.module';
import { ngIfFade } from './../../../util/animates.custons';
import { DialogService } from './../../../services/dialog.service';
import { URLs, oMsg } from './../../../util/values';
import { AppComponent } from './../../../app.component';
import {
   Component,
   OnInit
} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Router } from '@angular/router';


@Component({
  selector: 'app-listar-organizacoes',
  templateUrl: './listar-organizacoes.component.html',
  styleUrls: ['./listar-organizacoes.component.scss'],
  animations:[ngIfFade]
})
export class ListarOrganizacoesComponent implements OnInit{

  constructor(
    private app:AppComponent,
    private http: HttpClient,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private router: Router
  ) { }
  
  public dataTable:any;
  public fieldsSearch = {cnpj: "", nomeFantasia: ""};
  public search = {};
  public idBtnRemover= "idBtnRemoverOrganizacao";
  public idBtnEdit= "idBtnEditOrganizacao";
  public itemSelected: any;
  private textRemover = this.translateService.getValue("REMOVER");
  private textEditar = this.translateService.getValue("EDITAR");
  public textBtnPesq;
  public showFiltro: boolean = false;


  ngOnInit() {
    let titulo = this.translateService.getValue("TITLE_ORGANIZACOES");
    this.textBtnPesq = this.translateService.getValue("PESQUISAR");
    this.app.setTitle(titulo);
    this.listarFiltros();
    this.app.addActionFixed("idTeste", "search", this.textBtnPesq, 0, () => {
      this.showFiltro = !this.showFiltro;
    });
  }
  
  selected(item:any){
    if(item){
      console.log(item);
      this.app.addAction(this.idBtnRemover, "delete", this.textRemover,() => {
        this.remove();
      });
      
      this.app.addAction(this.idBtnEdit, "edit", this.textEditar,() => {
        this.edit();
      });
    }else{
      this.removeBtnItens();
    }
    this.itemSelected = item;
  }
  
  remove(){
    let nomeFantaisa = this.itemSelected["nomeFantasia"];
    let mensagem = this.translateService.getValue("DESEJA_EXCLUIR", nomeFantaisa);
    this.dialogService.confirm(null, mensagem, () => {
      this.removerOrganizacao(this.itemSelected["_id"]);
    }, null);
  }

  removerOrganizacao(id: any){
    this.dialogService.openLoading(null, 0);
    this.http.delete(URLs.localhost + URLs.ORGANIZACAO + "/" + id).subscribe(data => {
      this.dialogService.closeLoading();
      this.dialogService.showMsgData(data);
      this.listarFiltros();
      }, error => {
        this.dialogService.showErrorModal(error);
    });
  }


  edit(){
    this.router.navigate(['/cad-organizacao', this.itemSelected["_id"]]);
  }

  setPagination(search: any){
    this.search = search;
    this.listarFiltros();
  }
  
  limparFiltro(){
    this.fieldsSearch = {cnpj: "", nomeFantasia: ""};
  }

  listarFiltros(){
    this.dialogService.openLoading(null, 0);
    this.search[oMsg.SEARCH] = this.fieldsSearch;
    // this.http.post(URLs.localhost + URLs.ORGANIZACAO_PAGINATION, this.search).subscribe(data =>{
    //   this.dataTable = data;
    //   this.dialogService.closeLoading();
    //   console.log(data);
    // },error => {
    //   this.dialogService.showErrorModal(error);
    // });
    // this.removeBtnItens();
  }
 
  removeBtnItens(){
    this.app.removeAction(this.idBtnEdit);
    this.app.removeAction(this.idBtnRemover);
  }

  close(isShow){
    this.showFiltro = isShow;
  }
}
