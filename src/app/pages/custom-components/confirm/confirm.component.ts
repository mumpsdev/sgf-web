import { TranslateService } from './../../../util/translate/translate.service';
import { MzBaseModal } from 'ng2-materialize';
import {
   Component,
   OnInit,
   ViewChild,
   ElementRef,
   AfterViewChecked
 } from '@angular/core';

@Component({
  selector: 'm-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent extends MzBaseModal implements OnInit {
  constructor(private translate:TranslateService){
    super();
  }
  static title: string;
  static message: string;
  labelCancel: string; 
  static callbackOk: any;
  static callbackCancel: any;

  private btnOk: ElementRef;
  @ViewChild('btnOk') set setBtnOk(button: ElementRef) {
    this.btnOk = button;
 }
  
  ngOnInit() {
    this.labelCancel = this.translate.getValue("CANCELAR")
  }

  ngAfterViewChecked(){
    if(this.btnOk){
      this.btnOk.nativeElement.focus();
    }
  }
  
  get statictitle(){
    return ConfirmComponent.title;
  }
  
  get staticMessage(){
    return ConfirmComponent.message;
  }

  public getOk(){
    if(ConfirmComponent.callbackOk){
      ConfirmComponent.callbackOk();
    }
  }
  public getCancel(){
    if(ConfirmComponent.callbackCancel){
      ConfirmComponent.callbackCancel();
    }
  }

}