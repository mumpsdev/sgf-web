import { forEach } from '@angular/router/src/utils/collection';
import {
   Component,
   OnInit,
   Input,
   ElementRef,
   Renderer2
   } from '@angular/core';

@Component({
  selector: 'm-link-menu',
  templateUrl: './link-menu.component.html',
  styleUrls: ['./link-menu.component.scss']
})
export class LinkMenuComponent implements OnInit {
  
  @Input() acoes: any;

  menuLinks: Array<any> = [];
  
  constructor(
    private elementRef: ElementRef,
    private renderer2: Renderer2
  ) { }

  ngOnInit() {
    this.setMenu();
  }

  setMenu(){
    if(this.acoes){
      let acoesCopy = this.acoes.map(x => Object.assign({}, x));
      acoesCopy.forEach( acao => {
        if(!acao.id_no && acao.isMenu){
          this.menuLinks.push(acao);
        }else if(acao.id_no && acao.isMenu){
          let acaoExistente = this.menuLinks.find((ac) => ac.id == acao.id_no);
          if(acaoExistente){
            if(!acaoExistente.subMenus){
              acaoExistente.subMenus = new Array<any>();
            }
            acaoExistente.subMenus.push(acao);
          }
        }
      })
    }
  }


}
