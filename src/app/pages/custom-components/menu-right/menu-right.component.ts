import { DialogService } from './../../../services/dialog.service';
import { ngIfSlideOutRight, ngIfSlideInRight } from './../../../util/animates.custons';
import {
   Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    HostListener,
    Renderer2,
    ElementRef
  } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'm-menu-right',
  templateUrl: './menu-right.component.html',
  styleUrls: ['./menu-right.component.scss'],
  animations: [ngIfSlideOutRight, ngIfSlideInRight]
})
export class MenuRight implements OnInit {

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2,
    private dialogService: DialogService
  ) { 
    this.dialogService.addMenuRight(this);
  }

  ngOnInit() {
    if(!this.width){
      this.width="25rem";
    }
    this.dialogService.closeAllMenuRight(null);
    this.onInit = true;
  }
  onInit = false;
  _isShow: boolean = false;
  @Input() titleClass: string;
  @Input() title: string;
  @Input() width: string;
  @Input() full: boolean;
  @Input() fixed: boolean = false;
  @Input() modal: boolean = false;
  
  @Output() close: EventEmitter<any> = new EventEmitter();
  
  public isScroll: boolean = false;
  public animateContainer:any;
  public animateMove:any;
  
  public closeClick(){
    this.isShow = false;
    this.close.emit(this.isShow);
  }

  @Input()
  set isShow(value: boolean){
    this.setValue(value);
    this.setPaddingPage(value);
  
  }

  get isShow(){
    return this._isShow;
  }

  setValue(value: boolean){
    let isMenuOpen = this.dialogService.isMenuRightOpen(this);

    if(isMenuOpen){
      value = true;
    }else {
      if(this._isShow){
        value = false;
      }else if(this.onInit){
        value = true;
      }
    }
    this.dialogService.closeAllMenuRight(null);
    this._isShow = value;
  }
          
   setPaddingPage(value: boolean){
    let isMenuOpen = this.dialogService.isMenuRightOpen(null);
    let pages =  document.querySelectorAll(".page");
    if(isMenuOpen){
      for(let i = 0; i < pages.length; i++){
        this.renderer.setStyle(pages[i], "padding-right", this.width);
      }
      
    }else {
        for(let i = 0; i < pages.length; i++){
          this.renderer.setStyle(pages[i], "padding-right", "0.3rem");
        }
    }
  }

  public scrollSlideBody(event){
    if(event.target){
      if(event.target.scrollTop >= 10 && !this.isScroll){
        this.isScroll = true;
      }else if(event.target.scrollTop < 10 && this.isScroll){
        this.isScroll = false;
      }
    }
  }
}
