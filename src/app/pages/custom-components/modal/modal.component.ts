import { ngIfScale, ngIfFade, ngIfFadeOut } from './../../../util/animates.custons';
import { 
  Component,
  OnInit,
  ElementRef,
  Renderer2,
  HostListener,
  Output,
  Input,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'm-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations:[ngIfFadeOut, ngIfFade]
})
export class ModalComponent implements OnInit {

  @HostListener('window:resize', ['$event'])  onResize(event) {
    this.setScaleAndPositionContainer()
  }
  _isShow: boolean = false;
  inSetScale: boolean = false;
  heightWindow = 0;

  @Input() width: string;
  @Input() addClass: string;
  @Input() modal: boolean = false;

  @Output() close: EventEmitter<any> = new EventEmitter();

  constructor(
    private elementRef: ElementRef,
    private renderer2: Renderer2
  ) { }

  ngOnInit() {
    // this.timeSet();
  }
  
  get isShow(){
    return this._isShow;
  }
  
  @Input()
  set isShow(value: boolean){
    this._isShow = value;
    if(this._isShow){
      setTimeout(() => {
        this.elementRef.nativeElement.addEventListener('.modal-container:resize', this.setScaleAndPositionContainer());
        this.setScaleAndPositionContainer();
      }, 100);
    }
  }
  i = 0;
  setScaleAndPositionContainer(){
    this.i++;
    console.log(this.i);
    if(this._isShow){

      let padding = 0;
      if(this.heightWindow == 0 || (this.heightWindow < window.innerHeight)){
        padding = 30;
      }
      this.heightWindow = window.innerHeight;
      let container = this.elementRef.nativeElement.querySelector(".modal-container");
      let title = this.elementRef.nativeElement.querySelector(".modal-title");
      let body = this.elementRef.nativeElement.querySelector(".modal-body");
      let footer = this.elementRef.nativeElement.querySelector(".modal-footer");
      if(footer){
        this.renderer2.setStyle(container, "padding-bottom", footer.offsetHeight + "px");
      }
      let heightTitle =  parseInt(title ? title.offsetHeight : 0);
      let heightFooter = parseInt(footer ? footer.offsetHeight : 0);
      let heightBody = this.heightWindow - (heightTitle + heightFooter + (padding + (heightFooter * 0.6)));
      let heightContainer = container.offsetHeight;
      let topContainer = (window.innerHeight - container.offsetHeight);
      topContainer =  (topContainer/2) - ((heightFooter * 0.7) + padding);
      let soma = (heightBody + heightFooter);
      topContainer = topContainer <= 0 || soma >= (window.innerHeight - padding)
        ? 10  : topContainer;
      this.renderer2.setStyle(body, "max-height", heightBody + "px");
      this.renderer2.setStyle(container, "top", topContainer + "px");
    }
  }

  public closeClick(){
    this.isShow = false;
    this.close.emit(this.isShow);
  }
}
