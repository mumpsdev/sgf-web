import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'm-modal-body',
  templateUrl: './modal-body.component.html',
  styleUrls: ['./modal-body.component.scss']
})
export class ModalBodyComponent implements OnInit {

  isScroll: boolean = false;
  @Input() addClass: string;

  constructor() { }

  ngOnInit() {
  }

  public scrollSlideBody(event){
    if(event.target){
      if(event.target.scrollTop >= 10 && !this.isScroll){
        this.isScroll = true;
      }else if(event.target.scrollTop < 10 && this.isScroll){
        this.isScroll = false;
      }
    }
  }

}
