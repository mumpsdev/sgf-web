import { UtilService } from './../../../services/util.service';
import { AppComponent } from './../../../app.component';
import { DialogService } from './../../../services/dialog.service';
import { oMsg } from './../../../util/values';
import { ngIfFade, ngIfSlide, ngIfSlideOutRight } from './../../../util/animates.custons';
import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output, 
  ElementRef,
} from '@angular/core';
import { HeadTableComponent } from './head-table/head-table.component';
@Component({
  selector: 'm-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  animations:[ngIfFade, ngIfSlide, ngIfSlideOutRight]
})
export class TableComponent implements OnInit {

  constructor(
    private DialogService: DialogService,
    private app: AppComponent,
    private utilService: UtilService
  ) { 
    this.heads = new Array();
  }

  // --------- Entradas
  @Input() data:any;//Dados aonde tem 
  @Input() list: Array<any>;
  @Input() addClass: string;
  @Input() titleClass: string;
  @Input() dragulaName: string;
  @Input() isMult: boolean = false;
  @Input() isSelected: boolean = false;
  @Input() msgDragDrop: string;

  // --------- Saídas
  @Output() loadTable: EventEmitter<any> = new EventEmitter();
  @Output() onClick: EventEmitter<any> = new EventEmitter();
  @Output() ondbClick: EventEmitter<any> = new EventEmitter();

  private CRESCENTE = "asc";
  private DECRESCENTE = "desc";
  private asc = [];
  private desc = [];
  private FIELD_SORT_DEFAULT = "id";
  public search = {page: 1, limit: 10, asc: "", desc: ""};
  public heads: Array<HeadTableComponent>;
  private countClickSort:number = 1;
  public itemSelected: any;
  public listSelected: any;
  private idActionDelete = "idActionDeleteTable";
  private idActionEdit = "idActionEditTable";
  private idActionPesq = "idActionPesqTable";


  public addHeads(head: HeadTableComponent){
    this.heads.push(head);
  }
  
  ngOnInit() {
  }

  setLimit(select){
    let novoLimit = select.value;
    if(this.search.limit != novoLimit)
    this.search.limit = novoLimit;
    this.emitirSearch();
  }
  
  pageNext(){
    this.app.cleanActions();
    this.search.page = this.data.page;
    this.search.page = ++this.search.page;
    this.emitirSearch();
  }
  
  pageBack(){
    this.search.page = this.data.page;
    this.search.page = --this.search.page;
    this.emitirSearch();
  }
  
  setSort(head:string){
    if(this.asc.includes(head) && !this.desc.includes(head)){
      this.removeAsc(head);
      this.addDesc(head);
    }else if(!this.asc.includes(head) && this.desc.includes(head)){
      this.removeAsc(head);
      this.removeDesc(head);
    }else if(!this.asc.includes(head) && !this.desc.includes(head)){
      this.addAsc(head);
    }
    this.emitirSearch();
  }

  setSortAscDesc(){
    this.search.asc = this.asc.toString();
    this.search.desc = this.desc.toString();
  }

  addAsc(head: string){
    if(!this.asc.includes(head)){
      this.asc.push(head);
    }
  }
  addDesc(head: string){
    if(!this.desc.includes(head)){
      this.desc.push(head);
    }
  }

  removeAsc(head: string){
    if(this.asc.includes(head)){
      this.asc.splice(this.asc.indexOf(head), 1);
    }
  }

  removeDesc(head: string){
    if(this.desc.includes(head)){
      this.desc.splice(this.desc.indexOf(head), 1);
    }
  }

  inSort(head: string){
    return this.asc.includes(head) || this.desc.includes(head);
  }

  inAsc(head: string){
    return this.asc.includes(head);
  }
  inDesc(head: string){
    return this.desc.includes(head);
  }

  
  clickRow(item:any){
    if(this.isSelected){
      if(!this.isMult){
        if(this.itemSelected && this.itemSelected["selected"]){
          if(this.itemSelected == item){
            item.selected = false;
            this.itemSelected = null;
          }else{
            this.itemSelected["selected"] = false;
            item.selected = true;
            this.itemSelected = item;
          }
        }else{
          item.selected = true;
          this.itemSelected = item;
        }
        this.onClick.emit(this.itemSelected);
      }else{
        if(item["selected"]){
          item["selected"] = false;
        }else{
          item["selected"] = true;
        }
        this.listSelected = this.list || this.data[oMsg.LIST];
        this.listSelected = this.listSelected.filter( (item) => {return item["selected"]});
        this.onClick.emit(this.listSelected);
      }
    }
  }

  reload(){
    this.emitirSearch()
  }
  
  emitirSearch(){
    this.setSortAscDesc();
    this.loadTable.emit(this.search);
  }

  getValueField(value:object, field:string){
    return this.utilService.getValueObjectField(value, field);
  }

  clickBtn(head: HeadTableComponent, item: any){
    head.clickEmitter(item);
  }
}
