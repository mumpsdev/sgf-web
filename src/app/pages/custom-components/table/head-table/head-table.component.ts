import {
  Component,
  OnInit,
  Input,
  Optional,
  Host,
  ElementRef,
  Renderer2,
  Output,
  EventEmitter
} from '@angular/core';
import { TableComponent } from './../table.component';

@Component({
  selector: 'm-head-table',
  templateUrl: './head-table.component.html',
  styleUrls: ['./head-table.component.scss']
})
export class HeadTableComponent implements OnInit {

  constructor(
    @Optional() @Host() private table: TableComponent,
    private elementRef: ElementRef,
    private renderer2: Renderer2
  ) {
    this.table.addHeads(this);
   }
  //  ************************ Entradas **************
  @Input() label: string;
  @Input() field: string;
  @Input() sort: boolean = false;
  @Input() icon: string;
  @Input() image: string;
  @Input() btn: boolean;
  @Input() iconClass: string;
  @Input() btnClass: string;
  @Input() imageClass: string;
  @Input() btnTooltip: string;
  @Input() btnTooltipPosition: string;
  @Input() addClass: string;
  @Input() width: string; 
  
  //  ************************ Saídas **************
  @Output() onClick: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    
  }

  public  clickEmitter(item: any){
    this.onClick.emit(item);
  }

}
