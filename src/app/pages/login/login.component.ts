import { forEach } from '@angular/router/src/utils/collection';
import { DialogService } from './../../services/dialog.service';
import { TranslateService } from './../../util/translate/translate.service';
import { LoadingComponent } from './../custom-components/loading/loading.component';
import { UtilService } from './../../services/util.service';

import { URLs, oMsg, secretToken } from './../../util/values';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AppComponent } from './../../app.component';
import {
   Component,
   OnInit,
   AfterViewChecked,
   OnChanges,
   ViewChild,
   ElementRef
} from "@angular/core"

import 'rxjs/add/operator/map';
 import {
   ngIfSlide,
   ngIfScale
} from "../../util/animates.custons"
import {HttpErrorResponse} from '@angular/common/http';
import 'rxjs/add/operator/do';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],

  animations: [
    ngIfSlide, ngIfScale
  ]
})

export class LoginComponent implements OnInit, AfterViewChecked {
  
  constructor(
    private elementRef: ElementRef,
    private app: AppComponent,
    private http: HttpClient,
    private router: Router,
    private utilService: UtilService,
    private dialogService: DialogService,
    private translate: TranslateService
  ) { }

  user: any = {login: "", password: ""};
  inLogin = true;
  listMenu: Array<any> = new Array<any>();
  // Textos tela
  InformeLogin = this.translate.getValue("DIGITE_LOGIN");
  informeSenha = this.translate.getValue("DIGITE_SENHA");
  aguardeLogin = this.translate.getValue("AGUARDE_LOGIN");
  msgSenhaInvalida = this.translate.getValue("SENHA_INVALIDA");
  
  private inputSenha: ElementRef;
  private inputLogin: ElementRef;
  
   @ViewChild('inputLogin') set setLogin(login: ElementRef) {
      this.inputLogin = login;
   }
   @ViewChild('inputSenha') set setSenha(senha: ElementRef) {
      this.inputSenha = senha;

   }

  ngOnInit() {
    sessionStorage.removeItem(secretToken.TOKEN);
    this.user = {};
    this.app.userLogged = null;
    this.app.setIsLogged(false);
  }

  ngAfterViewChecked(){
    if(this.inLogin && this.inputLogin){
      this.inputLogin.nativeElement.focus();
    }else if(!this.inLogin && this.inputSenha){
      this.inputSenha.nativeElement.focus();
    }
  }
  
  showLogin(){
    this.inLogin = true;
  }
  
  showSenha(){
    if(this.user.login){
      this.dialogService.openLoading(this.aguardeLogin, 0);
      this.http.get(URLs.localhost + "/api/v1/getLoginAuth" + "/" + this.user.login).subscribe(data => {
          let userAuth = data[oMsg.OBJ];
          if(userAuth && userAuth["nome"]){
            this.user.nome = userAuth.nome
            this.inLogin = false;
          }else{
            this.dialogService.showMsgData(data);
          }
          this.dialogService.closeLoading();
        }, error => {
          this.dialogService.showErrorModal(error);
      });
    }
  }
  
  login(){
    if(this.user.password){
      this.dialogService.openLoading(this.aguardeLogin, 0);
      this.http.post(URLs.localhost + URLs.AUTH_AUTHENTICATE, this.user).subscribe(data => {
          let obj = data[oMsg.OBJ];
          if(obj[oMsg.COLABORADOR]){
            this.app.userLogged = obj[oMsg.COLABORADOR];
            this.app.setIsLogged(true);
          }
          if(obj[oMsg.ACOES]){
            this.app.listActions = obj[oMsg.ACOES];
          }
          if(obj[secretToken.TOKEN] && obj[secretToken.TOKEN] != "undefined"){
            sessionStorage.setItem(secretToken.TOKEN, obj[secretToken.TOKEN]);
            this.router.navigate(['/home']);
          }
          this.dialogService.closeLoading();
        }, error => {
          this.dialogService.closeLoading();
          this.app.isLogged = false;
          this.app.userLogged = null;
          if(error instanceof HttpErrorResponse){
            if(error.status == 401){  
              this.dialogService.alert(null, this.msgSenhaInvalida, null);
            }else{
              this.dialogService.showErrorModal(error);
            }
          }
      });
    }
  }
}
