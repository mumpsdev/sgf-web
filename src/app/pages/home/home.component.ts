import { oMsg, URLs } from './../../util/values';
import { HttpClient } from '@angular/common/http';
import { ngIfSlide, ngIfScale } from './../../util/animates.custons';
import { DialogService } from './../../services/dialog.service';
import { TranslateService } from './../../util/translate/translate.service';
import { AppComponent } from './../../app.component';
import {
   Component,
   OnInit,
   AfterViewChecked,
   Renderer2
} from '@angular/core';
import { ngIfFade } from '../../util/animates.custons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [ngIfFade, ngIfSlide, ngIfScale]
})
export class HomeComponent implements OnInit, AfterViewChecked {

  constructor(
    private app: AppComponent,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private http: HttpClient,
    private renderer: Renderer2
  ) { }

  listaFiliais = [
    {id: 1, fantasia: "Angelo Marques YES LTDA"}
  ];
  listaAnos: any;
  meses: any;
  filialSelecionada;
  filialNome;
  anoSelecionado;
  nomesMes = [
    "JANEIRO", "FEVEREIRO", "MARCO", "ABRIL",
    "MAIO", "JUNHO", "JULHO", "AGOSTO",
    "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"
  ]
  permissaoMostrarMeses = true;
  isAdmin = this.app.userLogged && this.app.userLogged.isAdmin;

  ngOnInit() {
    let home = this.translateService.getValue("HOME");
    this.app.setTitle(home);
    let acaoMostrarMeses = this.app.getAcao(URLs.LIST_ESCALAS_ANO_FILIAL, "list", "GET");
    if(!acaoMostrarMeses){
      this.permissaoMostrarMeses = false;
    }
    if(this.listaFiliais && this.listaFiliais.length >= 1){
      this.filialSelecionada = this.listaFiliais[0].id;
      this.filialNome = this.listaFiliais[0].fantasia;
      this.buscarAnosFilial();
    }
  }
  
  ngAfterViewChecked(){
    
  }

  buscarAnosFilial(){
    if(this.filialSelecionada){
      this.listaAnos = [];
      this.dialogService.openLoading(null, 0);
      this.http.get(URLs.localhost + "/api/v1/escala/filial/" + this.filialSelecionada).subscribe(data => {
        let obj = data[oMsg.OBJ];
        if(obj["anos"]){
          this.listaAnos = obj["anos"] || [];
          if(this.listaAnos.length >= 1){
            this.anoSelecionado =  this.listaAnos[(this.listaAnos.length-1)].ano;
            this.buscarMesAnoFilial();
          }
          this.meses = [];
        }
        this.dialogService.closeLoading();
      }, error => {
        this.dialogService.showErrorModal(error);
        this.dialogService.closeLoading();
     });
    }
  }

  buscarMesAnoFilial(){
    if(this.filialSelecionada && this.anoSelecionado){
      this.meses = [];
      this.dialogService.openLoading(null, 0);
      this.http.get(URLs.localhost + "/api/v1/escala/ano/" + this.anoSelecionado + "/filial/" + this.filialSelecionada).subscribe(data => {
        let obj = data[oMsg.OBJ];
        if(obj["mes"]){
          let meses = obj["mes"];
          this.carregarMeses(meses);
        }
        this.dialogService.closeLoading();
      }, error => {
        this.dialogService.showErrorModal(error);
        this.dialogService.closeLoading();
     });
    }
  }

  carregarMeses(meses: Array<any>){
    this.meses = [];
    let indexMes = 0;
    this.nomesMes.forEach((nomeMes, index) => {
      let idLoop = undefined, mesLoop = (index + 1), anoLoop = this.anoSelecionado, statusLoop = undefined, filialIdLoop = this.filialSelecionada, numMes = (index + 1);
      if(indexMes < meses.length && meses[indexMes].mes == numMes){
        idLoop = meses[indexMes].id;
        mesLoop = meses[indexMes].mes;
        statusLoop = meses[indexMes].status;
        anoLoop = meses[indexMes].ano;
        filialIdLoop = meses[indexMes].filialId;
        indexMes++;
      }
      this.meses.push({
        id: idLoop,
        mes: numMes,
        ano: anoLoop,
        nome: nomeMes,
        status: statusLoop,
        filialId: filialIdLoop,
        filialNome: this.filialNome,
        acoes: [{imgUrl: "assets/images/link-escala.png", tooltip: "ESCALA", link: "/escala-mes"}]
      })
    });
  }

  onChangeFilial(){
    this.buscarAnosFilial();
  }

  onChangeAnoMeses(){
    this.buscarMesAnoFilial();
  }
}