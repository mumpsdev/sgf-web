import { ListarOrganizacoesComponent } from './../../organizacao/listar-organizacoes/listar-organizacoes.component';
import { URLs, oMsg } from './../../../util/values';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogService } from './../../../services/dialog.service';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from './../../../util/translate/translate.service';
import { AppComponent } from './../../../app.component';
import { ngIfFade } from './../../../util/animates.custons';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastro-perfil',
  templateUrl: './cadastro-perfil.component.html',
  styleUrls: ['./cadastro-perfil.component.scss'],
  animations: [ngIfFade]
})
export class CadastroPerfilComponent implements OnInit {

  constructor(
    private app: AppComponent,
    private translateService: TranslateService,
    private http: HttpClient,
    private dialogService: DialogService,
    private router:Router,
    private route: ActivatedRoute

  ) { }
  perfil = {nome: "", descricao: "", organizacao: ""};
  public idPerfil: any;
  public listaOrganizacoes: Array<any>;


  ngOnInit() {
    let tituloPerfil = "TITLE_CAD_PERFIL";
    this.route.params.subscribe((params) => {
      this.idPerfil = params["id"];
    });
    if(this.idPerfil){
      tituloPerfil = "TITLE_EDIT_PERFIL";
      this.carregarPerfil();
    }
    tituloPerfil = this.translateService.getValue(tituloPerfil);
    this.app.setTitle(tituloPerfil);
    this.carregarOrganizacoes();
  }

  public carregarPerfil(){
    this.dialogService.openLoading(null, 0);
    this.http.get(URLs.localhost + URLs.PERFIL + "/" + this.idPerfil).subscribe(data => {
      this.dialogService.closeLoading();
        this.perfil = data[oMsg.OBJ];
        this.dialogService.showMsgData(data);
      }, error => {
        this.dialogService.showErrorModal(error);
    });
  }

  public carregarOrganizacoes(){
    this.http.get(URLs.localhost + URLs.ORGANIZACAO).subscribe(data =>{
      this.listaOrganizacoes = data[oMsg.LIST];
      this.dialogService.closeLoading();
    },error => {
      this.dialogService.showErrorModal(error);
    });
  }

  public atualizar(){
    this.http.put(URLs.localhost + URLs.PERFIL + "/" + this.idPerfil, this.perfil).subscribe(data => {
      this.dialogService.closeLoading();
        this.router.navigate(["/perfil"])        
        this.dialogService.showMsgData(data);
      }, error => {
        this.dialogService.showErrorModal(error);
    });

  }

  public salvar(){
    this.http.post(URLs.localhost + URLs.PERFIL, this.perfil).subscribe(data => {
      this.dialogService.closeLoading();
        this.router.navigate(["/perfil"])        
        this.dialogService.showMsgData(data);
      }, error => {
        this.dialogService.showErrorModal(error);
    });
  }

  limpar(){
    if(this.idPerfil){
      this.perfil = {nome: "", descricao: "", organizacao: this.perfil.organizacao};

    }else{
      this.perfil = {nome: "", descricao: "", organizacao: ""};
    }
  }


}
