import { UtilService } from './../../../services/util.service';
import { DragulaService } from './../../../util/ng2-dragula-custom/dragula.provider';
import { ngIfFade } from './../../../util/animates.custons';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { URLs, oMsg } from './../../../util/values';
import { AppComponent } from './../../../app.component';
import { DialogService } from './../../../services/dialog.service';
import { TranslateService } from './../../../util/translate/translate.service';
import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-listar-perfis',
  templateUrl: './listar-perfis.component.html',
  styleUrls: ['./listar-perfis.component.scss'],
  animations: [ngIfFade]
})
export class ListarPerfisComponent implements OnInit {

  constructor(
    private translateService: TranslateService,
    private utilService: UtilService,
    private dialogService: DialogService,
    private http: HttpClient,
    private router: Router,
    private app: AppComponent,
    private dragulaService: DragulaService,
    private renderer: Renderer2
  ) { 
    this.dragulaService.setDropIdUnique(true);
    let bag = this.dragulaService.find(this.dragDropAcoes);
    if(!bag){
      this.dragulaService.setOptions(this.dragDropAcoes, {
      removeOnSpill: false,
      revertOnSpill: true
    });

    this.dragulaService.out.subscribe( (value) => {
      let [e, el, container] = value;
      this.renderer.removeClass(el, "tr-selected");
      this.renderer.removeClass(el, "active");
    });
    
    this.dragulaService.drag.subscribe( (value) => {
      let [name, el, source] = value;
      this.renderer.addClass(el, "tr-selected");
    });
  }
}
  public dataTable:any;
  public fieldsSearch = {nome: "", page: "1", limit: "10", asc: "", desc: ""};
  public perfil: any = {acoes: []};
  public listaAcoes: any = [];
  public urlFilter = "";
  public idBtnRemover= "idBtnRemoverPerfil";
  public idBtnEdit= "idBtnEditPerfil";
  public itemSelected: any;
  private textRemover = this.translateService.getValue("REMOVER");
  private textEditar = this.translateService.getValue("EDITAR");
  public textBtnPesq;
  public tituloCad;
  public dragDropAcoes = "DRAG_DROP_ACOES";
  public msgDrag =  this.translateService.getValue("SOLTE_ACOES_AQUI");
  public showFiltro: boolean = false;
  public showCad: boolean = false;
  public showAction: boolean = false;

  ngOnInit() {
    let titulo = this.translateService.getValue("TITLE_PERFIL");
    this.textBtnPesq = this.translateService.getValue("PESQUISAR");
    this.app.setTitle(titulo);
    this.app.addActionFixed("idSearchPerfil", "search", this.textBtnPesq, 0, () => {
      this.showFiltro = !this.showFiltro;
    });
    this.urlFilter = "?page=1";
    // this.listarFiltros();

    let where = {id: 3};
  }

  selected(item:any){
    if(item){
      console.log(item);
      this.app.addAction(this.idBtnRemover, "delete", this.textRemover,() => {
        this.remove();
      });
      
      this.app.addAction(this.idBtnEdit, "edit", this.textEditar,() => {
        this.edit();
      });
    }else{
      this.removeBtnItens();
    }
    this.itemSelected = item;
  }
  
  remove(){
    let nomePerfil = this.itemSelected["nome"];
    let mensagem = this.translateService.getValue("DESEJA_EXCLUIR", nomePerfil);
    this.dialogService.confirm(null, mensagem, () => {
      this.removeItem(this.itemSelected["id"]);
    }, null);
  }

  removeItem(id: any){
    this.dialogService.openLoading(null, 0);
    this.http.delete(URLs.localhost + URLs.PERFIL + "/" + id).subscribe(data => {
      this.dialogService.closeLoading();
      this.dialogService.showMsgData(data);
      this.listarFiltros();
      }, error => {
        this.dialogService.showErrorModal(error);
    });
  }


  edit(){
    this.showCad = !this.showCad;
    this.http.get(URLs.localhost + URLs.PERFIL + this.urlFilter).subscribe(data =>{
      this.perfil = data["obj"];
      this.dialogService.closeLoading();
      
    },error => {
      this.dialogService.showErrorModal(error);
    });
  }

  atualizarDadosTabela(dados: any){
    this.fieldsSearch.page = dados.page;
    this.fieldsSearch.limit = dados.limit;
    this.fieldsSearch.asc = dados.asc;
    this.fieldsSearch.desc = dados.desc;
    this.listarFiltros();
  }

  createUrl(){
    this.urlFilter = "?limit=" + this.fieldsSearch.limit;
    this.urlFilter += "&page=" + this.fieldsSearch.page;
    this.urlFilter += "&asc=" + this.fieldsSearch.asc;
    this.urlFilter += "&desc=" + this.fieldsSearch.desc;
    this.urlFilter += "&nome=" + "%" + this.fieldsSearch.nome + "%";
  }
  
  limparFiltro(){
    this.fieldsSearch.nome = "";
  }

  listarFiltros(){
    this.dialogService.openLoading(null, 0);
    this.createUrl();
    this.http.get(URLs.localhost + URLs.PERFIL + this.urlFilter).subscribe(data =>{
      this.dataTable = data;
      console.log(data);
      this.dialogService.closeLoading();
    },error => {
      this.dialogService.showErrorModal(error);
    });
    this.removeBtnItens();
  }
 
  removeBtnItens(){
    this.app.removeAction(this.idBtnEdit);
    this.app.removeAction(this.idBtnRemover);
  }

  fecharPesquisa(isShow){
    this.limparFiltro();
  }

  mostrarCadPerfil(){
    this.showCad = !this.showCad;
    this.tituloCad = this.translateService.getValue("CAD_PERFIL");
  }

  fecharCad(isShow){
    this.showCad = !this.showCad;
    this.perfil = {acoes: []};
  }
  
  mostrarListaAcoes(){
    this.showAction = !this.showAction;
    this.listarTodasAcoes();
  }

  salvarCadastroPerfil(){
    if(this.perfil && this.perfil.id && this.perfil.id != 0){
      this.editarPerfil();
    }else{
      this.salvarPerfil();
    }
  }

  salvarPerfil(){

  }

  editarPerfil(){

  }
  
  listarTodasAcoes(){
    this.dialogService.openLoading(null, 0);
    let acoesPerfil: string[] = this.perfil["acoes"];
    let filtroIds: string = "?notin=";
    if(acoesPerfil && acoesPerfil.length > 0){
      acoesPerfil.forEach((acao, index) => {
        if(index > 0){
          filtroIds += ",";
        }
        filtroIds += acao["id"];
      });
    }
    this.http.get(URLs.localhost + URLs.ACAO_NOTIN + filtroIds).subscribe(data =>{
      this.listaAcoes = data["list"];
      this.dialogService.closeLoading();
    },error => {
      this.dialogService.showErrorModal(error);
    });
  }

  private criarAcoes(perfil: any) : any{
    console.log(perfil);
    let acoes = []
    if(perfil["perfilAcao"]){
        let perfilAcao: Array<any> = perfil["perfilAcao"];
        perfilAcao.forEach(pAcao => {
            if(pAcao["acao"]){
                acoes.push(pAcao["acao"]);
            }
        });
    }
    perfil["acoes"] = acoes;
    delete perfil["perfilAcao"];
    return perfil;
  }

  private criarPerfilAcao(perfil: any){
    let perfisAcoes = [];
    if(perfil["acoes"]){
      let acoes: Array<any> = perfil["acoes"];
      acoes.forEach(acao => {
        perfisAcoes.push({perfilId: perfil.id, acaoId: acao.id});
      });
    }
    perfil["perfilAcao"] = perfisAcoes;
    delete perfil["acoes"];
  }

  adicionarAcaoPerfil(item: any){
    this.utilService.changeItemList(this.listaAcoes, this.perfil.acoes, item);
  }
  
  removerAcaoPerfil(item: any){
    this.utilService.changeItemList(this.perfil.acoes, this.listaAcoes, item);
  }

}
