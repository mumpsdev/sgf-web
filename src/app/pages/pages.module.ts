import { DialogService } from './../services/dialog.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from './../util/translate/translate.module';
import {FormsModule} from "@angular/forms";
import {
  BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { HttpClientModule } from '@angular/common/http';
import {MaterializeModule, MzToastModule, MzToastService} from "ng2-materialize";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './../services/auth.interceptor';
import { AuthGuard } from './../services/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { UtilService } from './../services/util.service';
import {DragulaModule} from "../util/ng2-dragula-custom/dragular.module";
//Paginas 
import { HomeComponent } from './home/home.component'
import { LoginComponent } from './login/login.component';
import { ListarOrganizacoesComponent } from './organizacao/listar-organizacoes/listar-organizacoes.component';
import { AlertComponent } from './custom-components/alert/alert.component';
import { ConfirmComponent } from './custom-components/confirm/confirm.component';
import { LoadingComponent } from './custom-components/loading/loading.component';
import { TableComponent } from './custom-components/table/table.component';
import { HeadTableComponent } from './custom-components/table/head-table/head-table.component';
import { CadastroOrganizacaoComponent } from './organizacao/cadastro-organizacao/cadastro-organizacao.component';
import { MenuRight } from './custom-components/menu-right/menu-right.component';
import { ListarPerfisComponent } from './perfil/listar-perfis/listar-perfis.component';
import { CadastroPerfilComponent } from './perfil/cadastro-perfil/cadastro-perfil.component';
import { EscalaMesComponent } from './escala/escala-mes/escala-mes.component';
import { DiaComponent } from './escala/dia/dia.component';
import { DragulaService } from '../util/ng2-dragula-custom/dragula.provider';
import { LinkMenuComponent } from './custom-components/link-menu/link-menu.component';
import { ModalComponent } from './custom-components/modal/modal.component';
import { ModalTitleComponent } from './custom-components/modal/modal-title/modal-title.component';
import { ModalFooterComponent } from './custom-components/modal/modal-footer/modal-footer.component';
import { ModalBodyComponent } from './custom-components/modal/modal-body/modal-body.component';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    TranslateModule,
    HttpClientModule,
    DragulaModule,
    MaterializeModule.forRoot()
  ],
  declarations: [
    LoginComponent,
    HomeComponent,
    ListarOrganizacoesComponent,
    AlertComponent,
    ConfirmComponent,
    LoadingComponent,
    TableComponent,
    HeadTableComponent,
    CadastroOrganizacaoComponent,
    MenuRight,
    ListarPerfisComponent,
    CadastroPerfilComponent,
    EscalaMesComponent,
    DiaComponent,
    LinkMenuComponent,
    ModalComponent,
    ModalTitleComponent,
    ModalFooterComponent,
    ModalBodyComponent
  ],
  exports: [
    BrowserAnimationsModule,
    HttpClientModule,
    MaterializeModule,
    LoginComponent,
    HomeComponent,
    AlertComponent,
    ConfirmComponent,
    LoadingComponent,
    TableComponent,
    HeadTableComponent,
    MenuRight,
    DragulaModule,
    LinkMenuComponent,
    ModalComponent,
    ModalTitleComponent,
    ModalFooterComponent,
    ModalBodyComponent
  ],
  entryComponents: [
    ConfirmComponent,
    AlertComponent,
    LoadingComponent
  ],
  providers: [
    AuthGuard,
    UtilService,
    DialogService,
    MzToastService,
    DragulaService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ]
})
export class PagesModule { }
