import { HomeComponent } from './../../home/home.component';
import { ngIfSlide } from './../../../util/animates.custons';
import { URLs, oMsg } from './../../../util/values';
import { HttpClient } from '@angular/common/http';
import { UtilService } from './../../../services/util.service';
import { forEach } from '@angular/router/src/utils/collection';
import { DialogService } from './../../../services/dialog.service';
import { TranslateService } from './../../../util/translate/translate.service';
import { AppComponent } from './../../../app.component';
import { DragulaService } from "../../../util/ng2-dragula-custom";
import { 
  Component,
   OnInit ,
   HostListener,
   ViewChild,
   ElementRef,
   Renderer2
} from '@angular/core';
import {ActivatedRoute} from "@angular/router"
import {Subscription} from "rxjs/Rx"
import { ngIfFade } from '../../../util/animates.custons';
import { error } from 'util';
import { Location } from "@angular/common";

@Component({
  selector: 'app-escala-mes',
  templateUrl: './escala-mes.component.html',
  styleUrls: ['./escala-mes.component.scss'],
  animations:[ngIfFade, ngIfSlide]
})

export class EscalaMesComponent implements OnInit {

  clickDay = {numDia: 0, trabalhando: [],  folgando: [], isFeriado: false};
  mes = {id: 0, mes: 0, nome: "", ano: 0, status: 0, statusNome: "", filialId: 0, filialNome: "", semanas: [], folgas:[]};
  tooltipFolgas: string;
  tituloTrabalhando: string;
  tituloFolgando: string;
  isFixed = false;
  mostrarFolgas = false;
  mostrarTrabalhando = false;
  mostrarFolgando = false;
  mostrarHistorico = false;
  numDia: number;
  ultimaAcao: string;
  grupoDragDrop: string = "grupoDragDrop";
  idBtnFolgas = "alert-folgas";
  idBtnGerarEscala = "btnGerarEscala";
  idBtnFecharEscala = "btnFecharEscala";
  listaHistorico = []
  inscricao: Subscription;
  inscricaoNotDrop: Subscription;
  semEscala = false;
  status = {"0": "", '1': "ABERTO", "2": "LIBERADO", "3": "FECHADO", "4":  "CANCELADO", "5":  "REMOVIDO"};
  statusCanDrop = ['2'];
  isAdmin = this.app.userLogged &&  this.app.userLogged.isAdmin;

  @HostListener('window:scroll', ['$event'])
  public onWindowScroll(event: Event): void {
    let titles = this.elementRef.nativeElement.querySelector(".back-titles");
    if(titles){
      if(window.pageYOffset >= 10 && !this.isFixed){
        this.isFixed = true;
      }else if(window.pageYOffset < 10 && this.isFixed){
        this.isFixed = false;
      }
    }
  }

  constructor(
    private app: AppComponent,
    private renderer: Renderer2,
    private elementRef: ElementRef,
    private dialogService: DialogService,
    private dragulaService: DragulaService,
    private translateService: TranslateService,
    private utilService: UtilService,
    private activeRoute: ActivatedRoute,
    private http: HttpClient,
    private location: Location
    ) {
      this.dragulaService.setDropIdUnique(true);
      let bag = this.dragulaService.find(this.grupoDragDrop);
      if(!bag){
        this.dragulaService.setOptions(this.grupoDragDrop, {
        removeOnSpill: false,
        revertOnSpill: true
      });
    }

    this.dragulaService.over.subscribe( (value) => {
      let [e, el, container] = value;
      let bag = this.dragulaService.find(this.grupoDragDrop);
      let targetClick = this.dragulaService.getSourceClick();
      if(targetClick == container){
        this.renderer.setAttribute(container, "disabled", "true");
      }
      this.renderer.addClass(container, "drag-over");
    });
    this.dragulaService.out.subscribe( (value) => {
      let [e, el, container] = value;
      this.renderer.removeClass(container, "drag-over");
      this.renderer.removeAttribute(container, "disabled");
      this.renderer.removeClass(el, "tr-selected");
      this.renderer.removeClass(el, "active");
    });
    
    this.dragulaService.drag.subscribe( (value) => {
      let [name, el, source] = value;
      this.renderer.addClass(el, "tr-selected");
    });

    this.inscricaoNotDrop = this.dragulaService.notDrop.subscribe((value) => {
      let [dropElm, target, source] = value;
      let msg = this.translateService.getValue("NAO_DROP_MES", [this.mes.nome + "", this.mes.ano + "", this.mes.statusNome]);
      this.dialogService.toast(msg, 5000, null, null);
      this.renderer.removeClass(dropElm, "tr-selected");
      this.renderer.removeClass(dropElm, "active");
    });
    
    this.dragulaService.dropModel.subscribe((value) => {
      let [name, dropElm, dropElmModel, source, target, sourceModel, targetModel] = value;
      this.renderer.removeClass(dropElm, "tr-selected");
      this.renderer.removeClass(dropElm, "active");
      let index = this.dragulaService.getIndexSource(name, target);
      let bag = this.dragulaService.find(name);
      let numDia = bag.drake.containers[bag.drake.containers.indexOf(target)].id;
      let dia = this.getDia(numDia);
      let msg: string = "";
      let estado;
      let sourceClick = this.dragulaService.getSourceClick();
      if(dropElmModel && dia){
        let qtdColaboradorTrabalhando = this.utilService.getCountDuplicateList(dia.trabalhando, dropElmModel.id, "id");
        let qtdColaboradorFolgando = this.utilService.getCountDuplicateList(dia.folgando, dropElmModel.id, "id");
        //Se o mesmo colaborador está trabalhando e folgando no mesmo dia.
        if(qtdColaboradorTrabalhando == 1 && qtdColaboradorFolgando == 1 && targetModel){
          msg = this.translateService.getValue("COLABORADOR_UNIQUE_DIA", [dropElmModel.nome, dia.numDia]);
          this.utilService.removeItemList(targetModel, dropElmModel.id, "id");
        }
        //Se o colaborador está trabalhando duas vezes no mesmo dia.
        if(qtdColaboradorTrabalhando > 1){
          estado = this.translateService.getValue("TRABALHANDO");
          msg = this.translateService.getValue("COLABORADOR_UNIQUE", [dropElmModel.nome, estado, dia.numDia]);
          this.utilService.removeItemList(dia.trabalhando, dropElmModel.id, "id");
        }else if(qtdColaboradorFolgando > 1){//Se o colaborador está trabalhando duas vezes no mesmo dia.
          estado = this.translateService.getValue("FOLGANDO");
          msg = this.translateService.getValue("COLABORADOR_UNIQUE", [dropElmModel.nome, estado, dia.numDia]);
          this.utilService.removeItemList(dia.folgando, dropElmModel.id, "id");
        }
        if(msg){//Se aconteceu algum erro.
          this.dialogService.alert(null, msg, () => {
            if(sourceModel){
              sourceModel.push(dropElmModel);//Devoltendo o colaborador para o dia que ele estava locado.
            }
          });
        }else if(this.ultimaAcao == "FOG"){//Se a ação atual é colaboradores que estão com folgas sobrando.
          this.setQuantidadeFoltasColaborador(dropElmModel, sourceModel);
        }else if(numDia == this.numDia){
            if(target != sourceClick){
              let novaEscala = dropElmModel.tipoEscala == 2 ? 1 : 2;
              this.alterarTipoEscalaColaborador(dropElmModel, novaEscala, sourceModel);
            }
        }
      }
    });
  }

  ngOnInit() {
     this.inscricao = this.activeRoute.params.subscribe(mes => {
     this.dragulaService.removeNotDrop(this.grupoDragDrop);
     if(mes && mes.id){
         this.configurarMes(mes);
     }
    });
  }

  ngOnDestroy(){
    if(this.inscricao){
      this.inscricao.unsubscribe();
    }
    if(this.inscricaoNotDrop){
      this.inscricaoNotDrop.unsubscribe();
    }
  }

  setQuantidadeFoltasColaborador(dropElmModel: any, sourceModel: any){
    if(dropElmModel.quantidade && dropElmModel.quantidade > 1){
      dropElmModel.quantidade--;
      if(sourceModel){
        setTimeout( () => {
          sourceModel.push(dropElmModel);
          this.setCalcColaboradoresComFolgas();
        }, 300);
      }
    }else{
      this.setCalcColaboradoresComFolgas();
    }
  }

  public getDia(numDia): any{
    let diaMes;
    this.mes.semanas.forEach(mes => {
      mes.dias.forEach(dia => {
        if(dia.numDia == numDia){
          if(!diaMes){
            diaMes = dia;
          }
        }
      });
    });
    return diaMes;
  }

  onClickTrabalhando(retorno){
    let dia = retorno[0];
    this.clickDay = dia;
    let target = retorno[1];
    this.dragulaService.setSourceClick(target);
    let isOpen = this.dialogService.isMenuRightOpen(null);
    let numDia = dia["numDia"];
    if(!isOpen || this.ultimaAcao != "TRA"){
      this.mostrarTrabalhando = !this.mostrarTrabalhando;
    }
    this.ultimaAcao = "TRA";
    this.numDia = numDia;
  }

  onClickFolgando(retorno){
    let dia = retorno[0];
    this.clickDay = dia;
    let target = retorno[1];
    this.dragulaService.setSourceClick(target);
    let isOpen = this.dialogService.isMenuRightOpen(null);
    let numDia = dia["numDia"];
    if(!isOpen || this.ultimaAcao != "FOL"){
      this.mostrarFolgando = !this.mostrarFolgando;
    }
    this.ultimaAcao = "FOL";
    this.numDia = numDia;
  }

  onCloseFolgas(){
    this.mostrarFolgando = !this.mostrarTrabalhando;
    this.ultimaAcao = "FOG"
  }
  closeDias(){
    this.ultimaAcao = ""
    this.dragulaService.setSourceClick(null);
  }

  setCalcColaboradoresComFolgas(){
    let totalFolgas = 0;
    this.mes.folgas.forEach( colaborador => {
      totalFolgas += colaborador.quantidade;
    });
    if(totalFolgas == 0){
      this.app.removeActionFixed(this.idBtnFolgas);
    }else{
      this.app.setBadgesActionsFixed(this.idBtnFolgas, totalFolgas);
    }
  }

  configurarMes(mesSelected: any){
    this.mes.id = mesSelected.id;
    this.mes.mes = mesSelected.mes;
    this.mes.filialId = mesSelected.filialId;
    this.mes.ano = mesSelected.ano;
    this.mes.nome = this.translateService.getValue(mesSelected.nome);
    this.mes.filialNome = mesSelected.filialNome;
    this.mes.status = mesSelected.status;
    this.mes.statusNome = mesSelected == 0 ?  " (" + this.translateService.getValue(this.status[mesSelected.status]) + ")": "";
    if(mesSelected.status && mesSelected.status != "undefined"){
      this.carregarMes();
    }else{
      this.iniciarEscalaMes(mesSelected);
    }
  }

  carregarMes(){
    this.dialogService.openLoading(null, 0);
    this.http.get(URLs.localhost + "/api/v1/escala/ano/" + this.mes.ano + "/mes/" + this.mes.mes + "/filial/" + this.mes.filialId).subscribe(data => {
      let obj = data[oMsg.OBJ];
      if(obj["mes"]){
        let mes = obj["mes"][0];
        this.mes.status = mes.status;
        if(this.statusCanDrop.includes(this.mes.status+"")){
          this.dragulaService.removeNotDrop(this.grupoDragDrop);
        }else{
          this.dragulaService.addNotDrop(this.grupoDragDrop);
        }
        this.app.removeActionFixed(this.idBtnFecharEscala);
        this.app.removeActionFixed(this.idBtnGerarEscala);
        if(this.mes.status == 1 && this.isAdmin){
          this.setBtnGerarEscala();
        }else if(this.mes.status == 2 && this.isAdmin){
          this.setBtnFecharEscala();
        }
        //Inserindo Título
        this.setTitulo();
        let tooltipFolgas = this.translateService.getValue("FUNCIONARIO_FOLGAS");
        //Verificando as funcionalidades dependendo do status do mês.
        this.setScalas(mes);
      }
      this.dialogService.closeLoading();
    }, error => {
      this.dialogService.showErrorModal(error);
      this.dialogService.closeLoading();
    });
  }

  setScalas(mes: any){
    let numDias = 1;
    if(mes["semana"]){
      this.mes.semanas = [];
      let semanas = mes["semana"];
      semanas.forEach((se, indexSe) => {
        let semana = {
          id: se.id,
          label: se.label,
          dias: []
        }
        if(se["dia"]){
          let dias = se["dia"];
          dias.forEach((diaSemana, indexDia) => {
            //Verificando se a semana começa depois do primeiro dia da semana.
            if(indexDia == 0 && diaSemana.numDiaSemana > 1 && dias.length < 7){
              let complete = 7 - dias.length;
              for(let i  = 0; i < complete; i++){
                semana.dias.push({numDia: null, trabalhando: [], folgando: [], isFeriado: true});
              }
            }
            let dia = {
              id: diaSemana["id"],
              numDia: this.utilService.getDayInMonth(mes.ano, diaSemana["numDiaAno"]), 
              numDiaSemana: diaSemana["numDiaSemana"],
              numDiaAno: diaSemana["numDiaAno"],
              semanaId: diaSemana["semanaId"],
              trabalhando: [],
              folgando: []
            }
            if(diaSemana["escala"]){
              let escalas = diaSemana["escala"];
              escalas.forEach((escala, index) =>{
                let colaborador = escala["colaborador"];
                colaborador["idEscala"] = escala.id;
                colaborador["idDia"] = diaSemana["id"];
                colaborador["tipoEscala"] = escala["tipoEscala"];
                if(escala.tipoEscala == 1){
                  dia.trabalhando.push(colaborador);
                }if(escala.tipoEscala == 2){
                  dia.folgando.push(colaborador);
                }
              });
            }
            semana.dias.push(dia);
            numDias++;
          });
        }
        if(se["reserva"]){
          let reserva = se["reserva"];
          if(reserva){
            reserva.forEach((colaborador, index) => {
              this.addColaboradorReserva(colaborador);
            });
          }
        }
        //Verificando se a semana termina antes do setimo dia da semana.
        if(semana.dias.length < 7){
          for(let i = semana.dias.length; i < 7; i++){
            semana.dias.push({numDia: null, trabalhando: [], folgando: [], isFeriado: true});
          }
        }
        this.mes.semanas.push(semana);
      })
      if(this.mes.semanas.length < 1){
        this.semEscala = true;
      }
    }
    this.dialogService.closeLoading();
  }

  addColaboradorReserva(coloborador: any){
    let existente = this.mes.folgas.find((col) => col.id == coloborador.id);
    if(existente){
      existente.quantidade++;
    }else{
      coloborador["quantidade"] = 1;
      this.mes.folgas.push(coloborador);
    }
  }

  iniciarEscalaMes(mes: any){
      this.dialogService.openLoading(null, 0);
      let dados = {};
      dados["filial"] = mes.filialId;
      dados["mes"] = mes.mes;
      dados["ano"] = mes.ano;
      this.http.post(URLs.localhost + URLs.INICIAR_ESCALA_MES, dados).subscribe(data => {
        this.dialogService.closeLoading();
        let obj = data["obj"];
        if(obj){
          let mesAno = obj["mesAno"];
          if(mesAno && mesAno.id){
            this.mes.id = mesAno.id;
            this.mes.status = mesAno.status;
            this.setTitulo();
            let msg = this.translateService.getValue("DESEJA_ABRIR_MES", [this.mes.nome, this.mes.ano + ""]);
            this.dialogService.confirm(null, msg, () =>{
              this.gerarEscalaMes();
            }, () =>{
              this.setBtnGerarEscala();
              this.carregarMes();
            });
          }
        }
      }, error =>{
        this.dialogService.showErrorModal(error);
      });
  }
  
  gerarEscalaMes(){
    let msgLoading = this.translateService.getValue("GERANDO_ESCALA_AGUARDE");
    this.dialogService.openLoading(msgLoading, 0);
    if(this.mes.id && this.mes.id > 0){
      let dados = {};
      dados["id"] = this.mes.id;
      this.http.post(URLs.localhost + URLs.GERAR_ESCALA_MES, dados).subscribe(data => {
        this.dialogService.closeLoading();
        this.carregarMes();
        let msg = this.translateService.getValue("ESCALA_GERADA_SUCESSO", [this.mes.nome, this.mes.ano + ""]);
        this.dialogService.toast(msg, 3000, "black", null);
      }, error => {
        this.dialogService.showErrorModal(error);
      });
    }
  }

  fecharEscalaMes(){
    this.dialogService.openLoading(null, 0);
    if(this.mes.id && this.mes.id > 0){
      let dados = {};
      dados["id"] = this.mes.id;
      dados["novoStatus"] = 3
      this.http.put(URLs.localhost + URLs.ESCALA_MUDAR_STATUS, dados).subscribe(data => {
        this.dialogService.closeLoading();
        this.carregarMes();
        let msg = this.translateService.getValue("ESCALA_FECHADA_SUCESSO", [this.mes.nome, this.mes.ano + ""]);
        this.dialogService.toast(msg, 3000, "black", null);
      }, error => {
        this.dialogService.showErrorModal(error);
      });
    }
  }

  setTitulo(){
    this.mes.statusNome = this.mes.status != 0 ?  this.translateService.getValue(this.status[this.mes.status]) : "";
    this.app.setTitle(this.mes.filialNome + " - " + this.mes.ano + " - " + this.mes.nome + " - (" + this.mes.statusNome + ")");
  }

  setBtnGerarEscala(){
    let tooltip = this.translateService.getValue("GERAR_ESCALA");
    this.app.addActionFixed(this.idBtnGerarEscala, "lock_open", tooltip, 0, () =>{
      this.gerarEscalaMes();
    })
  }

  setBtnFecharEscala(){
    let tooltip = this.translateService.getValue("FECHAR_ESCALA");
    this.app.addActionFixed(this.idBtnFecharEscala, "lock_outline", tooltip, 0, () =>{
      this.fecharEscalaMes();
    })
  }

  alterarTipoEscalaColaborador(colaborador: any, novaEscala, sourceModel: Array<any>){
    this.dialogService.openLoading(null, 0);
    let dados = {id: colaborador.idEscala, colaboradorId: colaborador.id,
                 diaId: colaborador.idDia, tipoEscala: colaborador.tipoEscala,
                tipoEscalaNova: novaEscala};
    this.http.put(URLs.localhost + URLs.ESCALA_MUDAR_TIPO_TRABALHO_DIA_COLABORADOR, dados).subscribe(data => {
      this.dialogService.closeLoading();
      let msg = this.translateService.getValue("ESCALA_COLABORADOR_ATUALIZADA", colaborador["nome"]);
      this.dialogService.toast(msg, 3000, "black", null);
    }, error => {
      sourceModel.push(colaborador);
      this.dialogService.showErrorModal(error);
    });
  }

  mudaEscala(colaborador: any){
    let novaEscala = colaborador.tipoEscala == 1 ? 2 : 1;
    this.dialogService.openLoading(null, 0);
    let dados = {id: colaborador.idEscala, colaboradorId: colaborador.id,
                 diaId: colaborador.idDia, tipoEscala: colaborador.tipoEscala,
                tipoEscalaNova: novaEscala};
    this.http.put(URLs.localhost + URLs.ESCALA_MUDAR_TIPO_TRABALHO_DIA_COLABORADOR, dados).subscribe(data => {
      this.dialogService.closeLoading();
      this.trocarColaboradorLista(colaborador, novaEscala);
      let msg = this.translateService.getValue("ESCALA_COLABORADOR_ATUALIZADA", colaborador["nome"]);
      this.dialogService.toast(msg, 3000, "black", null);
    }, error => {
      this.dialogService.closeLoading();
      this.dialogService.showErrorModal(error);
    });
  }

  trocarColaboradorLista(colaborador: any, novaEscala: number){
    if(novaEscala == 1){
      let index = this.clickDay.folgando.indexOf(colaborador);
      if(index > -1){
        this.clickDay.folgando.splice(index, 1);
      }
      colaborador.tipoEscala = novaEscala;
      this.clickDay.trabalhando.push(colaborador);
    }else if(novaEscala == 2){
      let index = this.clickDay.trabalhando.indexOf(colaborador);
      if(index > -1){
        this.clickDay.trabalhando.splice(index, 1);
      }
      colaborador.tipoEscala = novaEscala;
      this.clickDay.folgando.push(colaborador);
    }
  }
}