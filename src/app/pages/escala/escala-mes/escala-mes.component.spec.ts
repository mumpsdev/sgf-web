import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EscalaMesComponent } from './escala-mes.component';

describe('EscalaMesComponent', () => {
  let component: EscalaMesComponent;
  let fixture: ComponentFixture<EscalaMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EscalaMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EscalaMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
