import { 
  Component,
   OnInit,
   Input,
   EventEmitter,
   Output
} from '@angular/core';
import { ngIfFade } from '../../../util/animates.custons';

@Component({
  selector: 'm-dia',
  templateUrl: './dia.component.html',
  styleUrls: ['./dia.component.scss'],
  animations: [ngIfFade]
})
export class DiaComponent implements OnInit {

  constructor() { 
  }
  ngOnInit() {
    
  }
  
  @Input() dia: {numDia: 0, trabalhando: Array<any>,  folgando: Array<any>, isFeriado};
  @Input() ultimaAcao: string;
  @Input() ativo: boolean;
  @Input() isAdmin: boolean;
  @Input() dragulaName: string;
  @Output() clickTrabalhando: EventEmitter<any> = new EventEmitter();
  @Output() clickFolgando: EventEmitter<any> = new EventEmitter();
  listaTrabalando = [];
  listaFolgando = [];


  onClickTrabalhando(dia, event){
    this.clickTrabalhando.emit([dia, event.target]);
  }
  
  onClickFolgando(dia, event){
    this.clickFolgando.emit([dia, event.target]);
  }

}
