import { forEach } from '@angular/router/src/utils/collection';
import { DialogService } from './services/dialog.service';
import {
  Component, 
  OnInit,
  AfterViewChecked,
  HostListener,
  ViewChild,
  ElementRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentFactory
} from "@angular/core"

import { URLs, oMsg, secretToken } from './util/values';
import { TranslateService } from './util/translate/translate.service';
import {HttpClient} from "@angular/common/http";
import {
  ngIfSlide,
  ngIfFade,
  fade,
  ngIfScale
} from "./util/animates.custons"
import { Router } from '@angular/router';
import {Location} from "@angular/common";
import { setTimeout } from 'timers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ngIfSlide, ngIfFade, fade, ngIfScale]
})

export class AppComponent implements OnInit, AfterViewChecked{
  constructor(
    private translate: TranslateService,
    private http: HttpClient,
    private router: Router,
    private location: Location,
    private resolver: ComponentFactoryResolver,
  ){}
  

  public tittle: string;
  public links = [];
  public actions = [];
  public actionsFixed = [];
  public userLogged: any;
  public listActions: any;
  public userActions: any;
  public toolbarShow = "out";
  public isRoot = true;
  public isLogged: boolean = false;
  private roots = ["/", "/home"];
  public textSair = this.translate.getValue("SAIR");
  public textMenuPrincipal: string;

  ngOnInit() {
    this.textMenuPrincipal = this.translate.getValue("MEMU_PRINCIPAL");
    this.setIsLogged(true);
    console.log(this.userLogged);
    if(this.userLogged){
      this.paginaAtiva(null);
    }else{
      this.router.navigate(["login"]);
    }

  }

  @ViewChild("navBar") navBar: ElementRef;
  
  ngAfterViewChecked(){
  }
  
  public setUserLogged(){
    if(!this.userLogged || !this.userLogged.login){
      this.http.get(URLs.localhost + URLs.AUTH_LOGGED).subscribe(data => {
        this.userLogged = data[oMsg.OBJ];
        this.isLogged = true;
      }, error => {
       this.router.navigate(["/login"]);
      });
    }
  }

  public logout(){
    this.isLogged = false;
    sessionStorage.removeItem(secretToken.TOKEN);
    this.router.navigate(["login"]);
    this.userLogged = null;
  }

  public paginaAtiva(page){
      let urlAtual =  this.router.url;
      if(this.roots.indexOf(urlAtual) > -1) {
        this.isRoot = true;
      }else{
        this.isRoot = false;
      }
      this.cleanActions();
      this.cleanActionsFixed();
  }

  public backPage(){
    this.location.back();
  }

  public setTitle(value: string){
    setTimeout(() => {
      this.tittle = "";
      setTimeout(() => {
        this.tittle = value;
      }, 150);
    }, 150);
  }
  
  public setIsLogged(value: boolean){
    setTimeout(() => {
      this.isLogged = value;
    }, 200);
  }

  public addActionFixed(id: string, icon: string, toolTip: string,  badge: number, action: Function){
    let notCan = this.actionsFixed.some( (a) => {
      return a.id == id 
    });
    if(!notCan){
      this.actionsFixed.push({id: id, icon: icon, tooltip: toolTip, action: action, badge: badge});
    }
  }

  public addAction(id: string, icon: string, toolTip: string, action: Function){
    let notCan = this.actions.some( (a) => {
      return a.id == id 
    });
    if(!notCan){
      this.actions.push({id: id, icon: icon, tooltip: toolTip, action: action});
    }
  }
  public clickAction(action: Function){
    if(action){
      action();
    }
  }

  public removeAction(id: string){
    this.actions = this.actions.filter((action) => {
      return action.id != id;
    });
  }

  public removeActionFixed(id: string){
    this.actionsFixed = this.actionsFixed.filter((action) => {
      return action.id != id;
    });
  }

  public setBadgesActionsFixed(id: string, newBadg: number){
    let actionFixed = this.actionsFixed.find((af) => af.id == id);
    if(actionFixed){
      actionFixed.badge = newBadg;
    }
  }
  
  public cleanActions(){
    this.actions = [];
  }
  public cleanActionsFixed(){
    this.actionsFixed = [];
  }

  public getAcao(url: string, item: string, metodo: string){
    let acao = null;
    if(this.listActions){
      this.listActions.forEach( a => {
        if((url && url == a.url) && (item && item == a.item) && (metodo && metodo == a.metodo)){
          acao = a;
        }
      });
    }
    return acao;
  }
}