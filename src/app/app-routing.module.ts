import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthGuard } from './services/auth.guard';
//Páginas
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { CadastroOrganizacaoComponent } from './pages/organizacao/cadastro-organizacao/cadastro-organizacao.component';
import { ListarOrganizacoesComponent } from './pages/organizacao/listar-organizacoes/listar-organizacoes.component';
import { CadastroPerfilComponent } from './pages/perfil/cadastro-perfil/cadastro-perfil.component';
import { ListarPerfisComponent } from './pages/perfil/listar-perfis/listar-perfis.component';
import { EscalaMesComponent } from './pages/escala/escala-mes/escala-mes.component';

const routes: Routes = [
  //Rotas principais
  { path: 'login', component: LoginComponent},
  { path: '', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  //,{ path: '**', component: LoginComponent}
  // Rotas controle de acesso
  {path: 'organizacao', component: ListarOrganizacoesComponent},
  { path: 'cad-organizacao/:id', component: CadastroOrganizacaoComponent, canActivate: [AuthGuard]},
  {path: 'perfil', component: ListarPerfisComponent},
  { path: 'cad-perfil', component: CadastroPerfilComponent, canActivate: [AuthGuard]},
  { path: 'cad-perfil/:id', component: CadastroPerfilComponent, canActivate: [AuthGuard]},
  //Escala
  { path: 'escala-mes', component: EscalaMesComponent, canActivate: [AuthGuard]}
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ],
  providers:[]
})
export class AppRoutingModule { }
