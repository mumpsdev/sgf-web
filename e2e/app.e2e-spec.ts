import { Teste5Page } from './app.po';

describe('teste5 App', () => {
  let page: Teste5Page;

  beforeEach(() => {
    page = new Teste5Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to m!');
  });
});
